#!/bin/bash
rm -f $1.epub
cd $1
zip -q0X "../$1.epub" mimetype
zip -qXr9D "../$1.epub" *
