from setuptools import setup, find_packages

setup(name='ePubber',
      version='0.1.0',
      author='Gabriele Mearelli',
      author_email='g.mearelli@gmail.com',
      license='',
      description="""
Command line tool to generate an eBook from a list of links""",
      packages=find_packages(),
      include_package_data=True,
      scripts=['epubber/generate_epub.py', 'publish.sh'],
      install_requires=['requests', 'BeautifulSoup4', 'jinja2',
                        'readability-lxml'])
