# -*- coding=utf-8 -*-
import logging
import os
import re
from urlparse import urljoin

from bs4 import BeautifulSoup
from jinja2 import Environment, FileSystemLoader
from readability.readability import Document
import requests

BASE_PATH = os.path.dirname(__file__)
PARSER_API = "https://readability.com/api/content/v1/parser"
CONTENT_PATH = "Content"
IMAGES_PATH = "Images"
env = Environment(loader=FileSystemLoader([
    os.path.join(BASE_PATH, 'templates')]))
logger = logging.getLogger(__name__)


class Config(object):
    def read(self, filename):
        self.filename = filename
        self.book_path = os.path.splitext(filename)[0]
        with open(self.filename, "r") as config_file:
            self.book_title = ""
            self.language = ""
            self.urls = []
            for line in map(lambda s: s.strip(), config_file.readlines()):
                m = re.match(r'^Title: (.*)', line)
                if m is not None:
                    self.book_title = m.group(1)
                    continue
                m = re.match(r'^Language: (.*)', line)
                if m is not None:
                    self.language = m.group(1)
                    continue
                self.urls.append(line)

config = Config()


class File(object):
    def get_path(self):
        raise NotImplementedError

    def save(self):
        with open(os.path.join(config.book_path, self.get_path()), "w") as f:
            f.write(self.content)


class Resource(File):
    def __init__(self, url, id):
        self.url = url
        self.id = id
        self.download()
        if self.is_complete():
            self.save()

    def is_complete(self):
        return hasattr(self, "content")


class Image(Resource):
    def download(self):
        try:
            resp = requests.get(self.url)
        except (requests.exceptions.RequestException,
                requests.exceptions.HTTPError) as e:
            logger.debug(e)
        else:
            self.content = resp.content
            self.content_type = resp.headers['content-type']

    def get_extension(self):
        return self.content_type.split('/')[1]

    def get_path(self):
        return os.path.join(IMAGES_PATH, "Image_{id}.{ext}".format(
            id=self.id,
            ext=self.get_extension()
            ))


class Article(Resource):
    def __init__(self, url, id):
        self.resources = []
        self.content_type = "application/xhtml+xml"
        super(Article, self).__init__(url, id)

    def download(self):
        j = {}
        try:
            resp = requests.get(self.url)
            resp.raise_for_status()
        except (requests.exceptions.RequestException,
                requests.exceptions.HTTPError) as e:
            logger.debug(e)
            j['error'] = True
        else:
            d = Document(resp.text)
            j['title'] = d.title()
            j['content'] = d.summary()
        if 'error' not in j:
            self.title = j['title']
            content = j['content']
            soup = BeautifulSoup(content, "html.parser")
            for i, img in enumerate(soup.findAll("img")):
                src = urljoin(self.url, img['src'])
                image = Image(src, id="{:d}_{:d}".format(self.id, i))
                if image.is_complete():
                    self.resources.append(image)
                    img['src'] = "../" + image.get_path()
            tmpl = env.get_template("Content/article.xhtml.tmpl")
            self.content = tmpl.render(
                    title=self.title,
                    id=self.id,
                    content=soup.encode(formatter="xml").decode("utf-8"),
                    language=config.language).encode("utf-8")

    def get_path(self):
        return os.path.join(CONTENT_PATH, "Article_{id}.xhtml".format(
            id=self.id,
            ))


class Index(File):
    def __init__(self, template, **kwargs):
        self.template = template
        self.tmpl = env.get_template(template)
        self.title = kwargs.get("title", "")
        self.id = kwargs.get("id", "")
        self.content = self.tmpl.render(**kwargs).encode("utf-8")
        self.resources = []
        self.save()

    def get_path(self):
        return os.path.splitext(self.template)[0]
