#!/usr/bin/env python
from datetime import date
import os
import sys
import shutil

from epubber.models import config, Article, Index, BASE_PATH, CONTENT_PATH
from epubber.models import IMAGES_PATH

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit("Usage {} mybook.txt".format(os.path.basename(sys.argv[0])))
    config_file = sys.argv[1]
    config.read(config_file)
    shutil.rmtree(config.book_path, ignore_errors=True)
    shutil.copytree(os.path.join(BASE_PATH, "book"), config.book_path)
    os.makedirs(os.path.join(config.book_path, CONTENT_PATH))
    os.makedirs(os.path.join(config.book_path, IMAGES_PATH))

    articles = [Article(url, i) for i, url in enumerate(config.urls)]
    articles = filter(lambda a: a.is_complete(), articles)
    index_pages = [
        Index("Content/cover.xhtml.tmpl",
              book_title=config.book_title,
              title="cover",
              id="cover",
              language=config.language),
        Index("Content/toc.xhtml.tmpl",
              title="Contents",
              id="toc",
              articles=articles,
              language=config.language)
    ]
    articles = index_pages + articles
    resources = list(articles)
    for article in articles:
        resources += article.resources
    Index("toc.ncx.tmpl",
          book_title=config.book_title,
          articles=articles)
    Index("content.opf.tmpl",
          book_title=config.book_title,
          articles=articles,
          resources=resources,
          language=config.language,
          date=date.today())
    os.system("publish.sh " + config.book_path)
    shutil.rmtree(config.book_path, ignore_errors=True)
