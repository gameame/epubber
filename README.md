# ePubber

**ePubber** is a command line tool to generate an eBook in ePub format from a list of links.

## Installation

On Ubuntu you should install:

    sudo apt-get install zlib1g-dev libxml2-dev libxslt1-dev python-dev

Then, install the script in a Python virtualenv:

    virtualenv v
    . v/bin/activate
    pip install git+https://bitbucket.org/gameame/epubber.git

## Usage

Create a file `epubber.txt` with this content:

    Title: ePubber
    Language: en
    https://bitbucket.org/gameame/epubber/
    http://idpf.org/epub/30
    https://github.com/javierarce/epub-boilerplate
    https://github.com/buriy/python-readability

Run the script:

    generate_epub.py epubber.txt